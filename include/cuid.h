#pragma once

#include <string>
#include <functional>
#include "flags.h"

#ifndef CUID_THREAD_SAFE
#define CUID_THREAD_SAFE 0
#endif

#if CUID_THREAD_SAFE
#include <atomic>
#include <mutex>
#endif

namespace cuid {
    class Generator {
    public:
        std::string cuid();
        std::string operator()() {
            return cuid();
        }

        explicit Generator(uint32_t flags = FLAGS::CUID_STANDARD);
        explicit Generator(std::function<size_t ()> rnd, unsigned flags = FLAGS::CUID_STANDARD);

        static size_t expected_length(uint32_t flags = FLAGS::CUID_STANDARD) {
            if (flags & FLAGS::CUID_STANDARD || !flags) {
                return 25;
            } else if (flags & FLAGS::CUID_LONG && flags & FLAGS::CUID_THREAD) {
                return 33;
            } else {
                return 29;
            }
        }

    private:
      const uint32_t flags;
      uint32_t counter = 0;
        const std::string fingerprint;
        const std::function<size_t (void)> rand_num;
#if CUID_THREAD_SAFE
        std::mutex rand_mux;
#endif
    };
}
