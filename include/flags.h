#pragma once

namespace cuid {
    enum FLAGS {
        CUID_STANDARD = 0x1 << 0,
        CUID_LONG = 0x1 << 1,
        CUID_THREAD = 0x1 << 2,
        CUID_REGENERATE_FINGERPRINT = 0x1 << 3,
    };
}
