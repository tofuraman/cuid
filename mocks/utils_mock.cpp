#include "../src/utils.h"
#include <string>

static std::string hostname = "host";

void set_hostname(const std::string& h) {
    hostname = h;
}

std::string get_hostname() {
    return hostname;
}

static size_t pid = 0;

void set_pid(size_t p) {
    pid = p;
}

size_t get_pid() {
    return pid;
}

static size_t thread_id = 0;

void set_thread_id(size_t tid) {
    thread_id = tid;
}

size_t get_thread_id() {
    return thread_id;
}

static size_t cur_time = 0;

void set_current_time(size_t t) {
    cur_time = t;
}

size_t get_current_time() {
    return cur_time;
}
