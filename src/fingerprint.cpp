#include <string>
#include "utils.h"
#include <algorithm>

char int_to_base_char (int integer, size_t base) {
    if (base == 64) {
        if (integer < 26) {
            return static_cast<char>('A' + integer);
        } else if (integer < 52) {
            return static_cast<char>('a' + (integer - 26));
        } else if (integer < 62) {
            return static_cast<char>('0' + (integer - 52));
        } else if (integer == 63) {
            return '+';
        } else {
            return '/';
        }
    }

    if (integer < 10) {
        return static_cast<char>('0' + integer);
    } else if (integer < 36) {
        return static_cast<char>('a' + (integer - 10));
    } else if (integer < 62) {
        return static_cast<char>('A' + (integer - 36));
    } else {
        return static_cast<char>('!' + (integer - 62));
    }
}

std::string to_base_36(size_t integer) {
    return to_base(integer, 36);
}



std::string to_base(size_t integer, size_t base) {
    std::string buffer;

    while (integer) {
        buffer += int_to_base_char(integer % base, base);
        integer /= base;
    }

    std::string result;
    result.resize(buffer.length());
    for (int i = 0; i < buffer.length(); ++i) {
        result[i] = buffer[buffer.length() - 1 - i];
    }

    if (base == 64) {
        while (result.length() % 4 != 0) {
            result += "=";
        }
    }

    return result;
}

std::string to_length(const std::string& str, size_t end_len) {
    std::string tmp;
    tmp.resize(end_len);
    for (int i = 0; i < str.length() && i < end_len; ++i) {
        tmp[tmp.length() - 1 - i] = str[str.length() - 1 - i];
    }
    if (str.length() < end_len) {
        for (int i = 0; i < end_len - str.length(); ++i) {
            tmp[i] = '0';
        }
    }
    return tmp;
}

std::string get_fingerprint(unsigned flags) {
    auto thread_id = get_thread_id();
    auto hostname = get_hostname();
    auto hostname_id = hostname.length() + 36;
    auto pid = get_pid();

    for (const auto c : hostname) {
        hostname_id += static_cast<unsigned>(c);
    }

    if (flags & cuid::CUID_STANDARD || !flags) {
        auto base_36_pid = to_length(to_base_36(pid), 2);
        auto host_fingerprint = to_length(to_base_36(hostname_id), 2);
        return base_36_pid + host_fingerprint;
    }
    else if (flags & cuid::CUID_THREAD) {
        if (flags & cuid::CUID_LONG) {
            auto base_36_pid = to_length(to_base_36(pid), 4);
            auto host_fingerprint = to_length(to_base_36(hostname_id), 4);

            return to_length(to_base_36(thread_id), 4) + base_36_pid + host_fingerprint;
        } else {
            auto base_36_pid = to_length(to_base_36(pid), 3);
            auto host_fingerprint = to_length(to_base_36(hostname_id), 3);

            return to_length(to_base_36(thread_id), 2) + base_36_pid + host_fingerprint;
        }
    } else {
        auto base_36_pid = to_length(to_base_36(pid), 4);
        auto host_fingerprint = to_length(to_base_36(hostname_id), 4);
        return base_36_pid + host_fingerprint;
    }
}
