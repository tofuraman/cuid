#if defined WIN32
#ifndef __WIN32__
#define __WIN32__ WIN32
#endif
#endif

#if defined (__WIN32__)
#include <Winsock2.h>
#include <processthreadsapi.h>
#else
#include <unistd.h>
#endif
#include "utils.h"
#include <thread>
#include <chrono>

std::string get_hostname() {
    constexpr int buffer_size = 256;

    #if defined (__WIN32__)
        char szPath[buffer_size] = "";
        WSADATA wsaData;
        WSAStartup(MAKEWORD(2, 2), &wsaData);
        gethostname(szPath, buffer_size);
        WSACleanup();
        return std::string(szPath);
    #else
        char buffer[buffer_size];
        if (!gethostname(buffer, buffer_size)) {
            return "host";
        } else {
            return std::string(buffer);
        }
    #endif
}

size_t get_pid() {
#if defined (__WIN32__)
    return GetCurrentProcessId();
#else
    return getpid();
#endif
}

size_t get_thread_id() {
    return std::hash<std::thread::id>{}(std::this_thread::get_id());
}

size_t get_current_time() {
    auto time = std::chrono::high_resolution_clock::now();
    return time.time_since_epoch().count();
}
